var gutil = require('gulp-util');
var nunjucks = require('nunjucks');
var through = require('through2');
var util = require('util');

module.exports = function (options, paths) {
    var env = nunjucks.configure(paths);

    return through.obj(function (file, enc, cb) {
        if (file.isNull()) {
            this.push(file);
            return cb();
        }

        if (file.isStream()) {
            this.emit('error',
                      new gutil.PluginError('gulp-nunjucks',
                                            'Streaming not supported'));
            return cb();
        }

        var opts = options || {};

        if (typeof opts === 'function') {
            opts = opts(file);
        }

        try {
            file.contents = new Buffer(env.renderString(file.contents.toString(), opts));
            file.path = file.path.replace('.dtl', '');
        } catch (err) {
            this.emit('error', new gutil.PluginError('gulp-nunjucks', err));
        }

        this.push(file);
        return cb();
    });
};
